/*
 * WEP encryption module.
 *
 * Part B: Attpmting to minimize area*delay product.
 *
 * http://en.wikipedia.org/wiki/RC4
 *
 * By Kenyon Ralph <kralph@ucsd.edu>
 */

module wep_encrypt (
    clk,
    nreset,
    start_encrypt,
    plain_addr,
    frame_size,
    cipher_addr,
    seed_msw,
    seed_lsw,
    done,
    port_A_clk,
    port_A_data_in,
    port_A_data_out,
    port_A_addr,
    port_A_we
);

    input clk;
    input nreset;

    input [31:0] start_encrypt;
    // Tells wep_encrypt to start encrypting the given frame.

    input [31:0] plain_addr;
    // Starting address of the plaintext frame. That is, specifies from where
    // wep_encrypt must read the plaintext frame.

    input [31:0] frame_size;
    // Length of the frame in bytes.

    input [31:0] cipher_addr;
    // Starting address of the ciphertext frame. That is, specifies where
    // wep_encrypt must write the ciphertext frame.

    input [31:0] seed_msw;
    // Contains the 4 most significant bytes of the 64 bit seed.

    input [31:0] seed_lsw;
    // Contains the 4 least significant bytes of the 64 bit seed.

    input [31:0] port_A_data_out;
    // Read data from the dpsram (plaintext).

    output [31:0] port_A_data_in;
    // Write data to the dpsram (ciphertext).

    output [31:0] port_A_addr;
    // Address of dpsram being read/written.

    output port_A_clk;
    // Clock to dpsram (drive this with the input clk).

    output port_A_we;
    // Write enable for dpsram.

    output done;
    // Signal to indicate that encryption of the frame is complete.

    reg		done;
    reg [4:0]	state;
    reg [4:0]	previous_state;
    reg [31:0]	encrypted_bytes;
    reg [31:0]	i;
    reg [31:0]	j;
    reg [31:0]	index;
    reg [7:0]	temp;
    reg [7:0]	S [0:255];
    reg [31:0]	plainword;
    reg [7:0]	plainbyte3;
    reg [7:0]	plainbyte2;
    reg [7:0]	plainbyte1;
    reg [7:0]	plainbyte0;
    reg [31:0]	cipherword;
    reg [7:0]	cipherbyte3;
    reg [7:0]	cipherbyte2;
    reg [7:0]	cipherbyte1;
    reg [7:0]	cipherbyte0;
    reg [31:0]	port_A_data_in;
    reg [31:0]	port_A_addr;
    reg		port_A_we;
    reg [31:0]	current_cipher_addr;
    reg [31:0]	current_plain_addr;
    reg [7:0]	seedbytes [0:7];

    assign port_A_clk	=	clk;

    // States.
    parameter RESET	     = 'd0;
    parameter KSAINIT	     = 'd1; // key-scheduling algorithm
    parameter KSASHUFFLE1    = 'd2;
    parameter KSASHUFFLE2    = 'd3;
    parameter PREREAD	     = 'd4;
    parameter READWORD	     = 'd5;
    parameter PRGA	     = 'd6; // pseudo-random generation algorithm
    parameter CRYPTBYTE3     = 'd7;
    parameter CRYPTBYTE2     = 'd8;
    parameter CRYPTBYTE1     = 'd9;
    parameter CRYPTBYTE0     = 'd10;
    parameter PREWRITE	     = 'd11;
    parameter WRITEWAIT	     = 'd12;
    parameter WRITEWORD	     = 'd13;
    parameter END	     = 'd14;
    parameter SEEDBYTESINIT  = 'd15;
    parameter CRYPT_SET_I    = 'd16;
    parameter CRYPT_SET_J    = 'd17;
    parameter S_SWAP	     = 'd18;
    parameter CRYPT_SET_INDEX= 'd19;
    parameter CRYPT_XOR	     = 'd20;

    always @(posedge clk or negedge nreset)
    begin
	if (!nreset)
	begin
	    port_A_addr <= 0;
	    port_A_we <= 0;
	    port_A_data_in <= 0;
	    current_plain_addr <= 0;
	    current_cipher_addr <= 0;
	    state <= RESET;
	end

	else
	begin
	    case (state)
		RESET:
		begin
		    //$display("   [RESET] current_plain_addr=%2d\n", current_plain_addr);
		    encrypted_bytes <= 0;
		    done <= 0;
		    i = 0;
		    j = 0;

		    if (start_encrypt == 1)
		    begin
			current_plain_addr <= plain_addr;
			current_cipher_addr <= cipher_addr;
			state <= SEEDBYTESINIT;
		    end
		end

		SEEDBYTESINIT:
		begin
		    seedbytes[0] <= seed_lsw[7:0];
		    seedbytes[1] <= seed_lsw[15:8];
		    seedbytes[2] <= seed_lsw[23:16];
		    seedbytes[3] <= seed_lsw[31:24];
		    seedbytes[4] <= seed_msw[7:0];
		    seedbytes[5] <= seed_msw[15:8];
		    seedbytes[6] <= seed_msw[23:16];
		    seedbytes[7] <= seed_msw[31:24];

		    state <= KSAINIT;
		end

		KSAINIT:
		begin
		    for (i = 0; i < 256; i = i + 1)
		    begin
			S[i] = i;
		    end

		    i = 0;
		    state <= KSASHUFFLE1;
		end

		KSASHUFFLE1: // i even
		begin
		    //$display("   [KSASHUFFLE1] i=%3d, j=%3d ", i, j);
		    j = (j + S[i] + seedbytes[i % 8]) % 256;

		    temp = S[i];
		    S[i] = S[j];
		    S[j] = temp;

		    i = i + 1;

		    state <= KSASHUFFLE2;
		    //$display("end of KSASHUFFLE1.\n");
		end

		KSASHUFFLE2: // i odd
		begin
		    //$display("   [KSASHUFFLE2] i=%3d\n", i);
		    j = (j + S[i] + seedbytes[i % 8]) % 256;

		    temp = S[i];
		    S[i] = S[j];
		    S[j] = temp;

		    i = i + 1;

		    //$display("   [KSASHUFFLE2] before if block i=%3d\n", i);
		    if (i == 256)
		    begin
			//$display("   [KSASHUFFLE2] going to PREREAD\n");
			j = 0;
			state <= PREREAD;

			/*
			for (i = 0; i < 256; i = i + 1)
			begin
			    $display("   [KSASHUFFLE2] S[%3d]=%3d\n", i, S[i]);
			end
			$stop;
			*/

		    end
		    else
		    begin
			//$display("   [KSASHUFFLE2] going to KSASHUFFLE1\n");
			state <= KSASHUFFLE1;
		    end
		end

		PREREAD:
		begin
		    //$display("   [PREREAD] reading from current_plain_addr: %3d\n", current_plain_addr);
		    port_A_addr <= current_plain_addr;
		    state <= READWORD;
		end

		READWORD:
		begin
		    plainword <= port_A_data_out;
		    current_plain_addr <= current_plain_addr + 32'd4;
		    state <= PRGA;
		end

		PRGA:
		begin
		    //$display("   [PRGA] plainword: %h\n", plainword);
		    // {cipher,plain}byte3 is the least-significant byte in
		    // the word.
		    plainbyte3 <= plainword[7:0];
		    plainbyte2 <= plainword[15:8];
		    plainbyte1 <= plainword[23:16];
		    plainbyte0 <= plainword[31:24];
		    port_A_addr <= current_cipher_addr;
		    state <= CRYPTBYTE3;
		end

		CRYPTBYTE3:
		begin
		    previous_state <= CRYPTBYTE3;

		    if (encrypted_bytes < frame_size)
		    begin
			i = encrypted_bytes;
			state <= CRYPT_SET_I;
		    end
		    else
		    begin
			cipherword[7:0] <= plainbyte3;
			state <= PREWRITE;
		    end
		end

		CRYPTBYTE2:
		begin
		    previous_state <= CRYPTBYTE2;

		    if (encrypted_bytes < frame_size)
		    begin
			i = encrypted_bytes;
			state <= CRYPT_SET_I;
		    end
		    else
		    begin
			cipherword[15:8] <= plainbyte2;
			state <= PREWRITE;
		    end
		end

		CRYPTBYTE1:
		begin
		    previous_state <= CRYPTBYTE1;

		    if (encrypted_bytes < frame_size)
		    begin
			i = encrypted_bytes;
			state <= CRYPT_SET_I;
		    end
		    else
		    begin
			cipherword[23:16] <= plainbyte1;
			state <= PREWRITE;
		    end
		end

		CRYPTBYTE0:
		begin
		    previous_state <= CRYPTBYTE0;

		    if (encrypted_bytes < frame_size)
		    begin
			i = encrypted_bytes;
			state <= CRYPT_SET_I;
		    end
		    else
		    begin
			cipherword[31:24] <= plainbyte0;
			state <= PREWRITE;
		    end
		end

		CRYPT_SET_I:
		begin
		    i = (i + 1) % 256;
		    state <= CRYPT_SET_J;
		end

		CRYPT_SET_J:
		begin
		    j = (j + S[i]) % 256;
		    state <= S_SWAP;
		end

		S_SWAP:
		begin
		    temp = S[i];
		    S[i] = S[j];
		    S[j] = temp;
		    state <= CRYPT_SET_INDEX;
		end

		CRYPT_SET_INDEX:
		begin
		    index <= (S[i] + S[j]) % 256;
		    state <= CRYPT_XOR;
		end

		CRYPT_XOR:
		begin
		    case (previous_state)
			CRYPTBYTE3:
			begin
			    cipherword[7:0] <= S[index] ^ plainbyte3;
			    encrypted_bytes <= encrypted_bytes + 'd1;
			    state <= CRYPTBYTE2;
			end

			CRYPTBYTE2:
			begin
			    cipherword[15:8] <= S[index] ^ plainbyte2;
			    encrypted_bytes <= encrypted_bytes + 'd1;
			    state <= CRYPTBYTE1;
			end

			CRYPTBYTE1:
			begin
			    cipherword[23:16] <= S[index] ^ plainbyte1;
			    encrypted_bytes <= encrypted_bytes + 'd1;
			    state <= CRYPTBYTE0;
			end

			CRYPTBYTE0:
			begin
			    cipherword[31:24] <= S[index] ^ plainbyte0;
			    encrypted_bytes <= encrypted_bytes + 'd1;
			    state <= PREWRITE;
			end
		    endcase
		end

		PREWRITE:
		begin
		    //$display("   [PREWRITE] encrypted_bytes: %2d (after possible encryptions)\n", encrypted_bytes);

		    //$display("   [PREWRITE] writing cipherword=%h to current_cipher_addr=%3d\n\n",
			     //cipherword, current_cipher_addr);

		    port_A_we <= 1;
		    port_A_data_in <= cipherword;
		    state <= WRITEWAIT;
		end

		WRITEWAIT:
		begin
		    state <= WRITEWORD;
		end

		WRITEWORD:
		begin
		    port_A_we <= 0;
		    port_A_addr <= current_plain_addr;
		    current_cipher_addr <= current_cipher_addr + 32'd4;

		    if (encrypted_bytes == frame_size)
		    begin
			state <= END;
		    end
		    else
		    begin
			state <= PREREAD;
		    end
		end

		END:
		begin
		    //$display("   [END]: done.\n");
		    done <= 1;
		    state <= RESET;
		end
	    endcase
	end
    end

endmodule
